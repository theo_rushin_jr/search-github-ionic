import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ProfileProvider } from '../../providers/profile/profile';
import { LoaderProvider } from '../../providers/loader/loader';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  profileName: string;
  gists: any = [];

  showLoader: boolean;

  constructor(public navCtrl: NavController, private profileProvider: ProfileProvider, private loaderProvider: LoaderProvider) {

  }

  ionViewDidLoad() {
    this.loaderProvider.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });
  }

  ionViewWillEnter() {
    if(this.profileProvider.getUsername()) {
      this.profileName = this.profileProvider.getName();

      this.loaderProvider.display(true);

      this.profileProvider.getProfileGists().subscribe(gists => {
console.log('gists: ', gists);
        this.gists = gists;

        this.loaderProvider.display(false);
      });
    }
  }

}
