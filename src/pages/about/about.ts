import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import moment from 'moment';

import { ProfileProvider } from '../../providers/profile/profile';
import { LoaderProvider } from '../../providers/loader/loader';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  profileName: string;
  repos: any = [];

  showLoader: boolean;

  constructor(public navCtrl: NavController, private profileProvider: ProfileProvider, private loaderProvider: LoaderProvider) {
    
  }

  ionViewDidLoad() {
    this.loaderProvider.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });
  }

  ionViewWillEnter() {
    if(this.profileProvider.getUsername()) {
      this.profileName = this.profileProvider.getName();

      this.loaderProvider.display(true);

      this.profileProvider.getProfileRepos().subscribe(repos => {
        let today = moment();

        this.repos = repos.map(repo => {
          let created = moment.utc(repo.created_at);

          let diff = today.diff(created, 'days');

          repo.days_since = diff;

          return repo;
        });

        // sort by value
        this.repos.sort(function (a, b) {
          return a.days_since - b.days_since;
        });

        this.loaderProvider.display(false);
      });
    }
  }
}
