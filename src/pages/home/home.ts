import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ProfileProvider } from '../../providers/profile/profile';
import { LoaderProvider } from '../../providers/loader/loader';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  profile: any[];
  repos: any[];
  gists: any[];
  username: string;

  showLoader: boolean;

  constructor(public navCtrl: NavController, private profileProvider: ProfileProvider, private loaderProvider: LoaderProvider) {

  }

  ionViewDidLoad() {
    this.loaderProvider.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });
  }

  findProfile(ev){
    if(ev.keyCode == 13) {
console.log('Enter key pressed: ', this.username);
      this.loaderProvider.display(true);

      this.profileProvider.setProfile(this.username);

      this.profileProvider.getProfileInfo().subscribe(profile => {
console.log('profile: ', profile);
        this.profile = profile;

        this.profileProvider.setName(profile.name);

        this.loaderProvider.display(false);
      });
    }
  }

}
